package anderes;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import model.Model;


public class Graphics {


    private GraphicsContext gc;
    private Model model;

    Image background = new Image("Bild/Bild.PNG", 0, 700, true, true);


    public Graphics(GraphicsContext gc, Model model) {
        this.gc = gc;
        this.model = model;
    }


    public void draw() {

        gc.clearRect(0, 0, model.WIDTH, model.HEIGHT);


        gc.drawImage(background, 0, 0);


        gc.setFill(Color.DEEPPINK);
        gc.fillOval(model.getPlayer().getX(), model.getPlayer().getY(),
                model.getPlayer().getW(), model.getPlayer().getH()
        );


        gc.setFill(Color.BLACK);
        gc.fillRect(model.getPaddle().getX(), model.getPaddle().getY(),
                model.getPaddle().getW(), model.getPaddle().getH()
        );

        gc.setFill(Color.BLACK);
        gc.fillRect(model.getPaddle2().getX(), model.getPaddle2().getY(),
                model.getPaddle2().getW(), model.getPaddle2().getH()
        );






    }


}
