package anderes;

import javafx.scene.input.KeyCode;
import model.Model;

public class InputHandler {


    private Model model;

    public InputHandler(Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyCode keyCode) {


        if (keyCode == KeyCode.LEFT) {
            model.getPaddle().move(-30, 0);
        }
        if (keyCode == KeyCode.RIGHT) {
            model.getPaddle().move(30, 0);
        }


    }

}
