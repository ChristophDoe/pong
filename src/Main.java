import anderes.Graphics;
import anderes.InputHandler;
import anderes.Timer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;
import model.Model;

public class Main extends Application {

    private Model model;

    @Override
    public void start(Stage stage) throws Exception {

        Model model = new Model();
        Canvas canvas = new Canvas(model.WIDTH, model.HEIGHT);
        Group group = new Group();
        group.getChildren().add(canvas);

        Scene scene = new Scene(group);

        stage.setScene(scene);

        GraphicsContext gc = canvas.getGraphicsContext2D();


        Graphics graphics = new Graphics(gc, model);

        Timer timer = new Timer(graphics, model);
        timer.start();

        InputHandler inputHandler = new InputHandler(model);

        scene.setOnKeyPressed(
                event -> inputHandler.onKeyPressed(event.getCode())
        );


        stage.show();


    }


}
