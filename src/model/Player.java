package model;


public class Player {

    private int x;
    private int y;
    private int h;
    private int w;
    private float speedX;
    private float speedY;


    public Player(int x, int y) {
        this.x = x;
        this.y = y;
        this.h = 45;
        this.w = 45;
        this.speedX = 5.0F;
        this.speedY = 5.0F;
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }


    public float getSpeedX() {
        return speedX;
    }

    public void setSpeedX(float speedX) {
        this.speedX = speedX;
    }


    public float getSpeedY() {
        return speedY;
    }

    public void setSpeedY(float speedY) {
        this.speedY = speedY;
    }

    public void update(long elapsedTime) {
        this.x = Math.round(this.x + elapsedTime * this.speedX);
        this.y = Math.round(this.y + elapsedTime * this.speedY);
    }

}






