package model;

public class Model {

    private Player player;
    private Paddle paddle;
    private Paddle paddle2;
    private boolean spielende;


    public static final double WIDTH = 800;
    public static final double HEIGHT = 700;
    public static final String ANSI_RED = "\u001B[31m";


    public Model() {
        this.player = new Player(0, 200);
        this.paddle = new Paddle(300, 660, 40, 250, 10.0F);
        this.paddle2 = new Paddle(300, 0, 40, 250, 10.0F);
        this.spielende = false;



    }

    public Player getPlayer() {
        return player;
    }

    public Paddle getPaddle() {
        return paddle;
    }

    public Paddle getPaddle2() {
        return paddle2;
    }


    public void update() {

        if (spielende != true) {


            player.update(1);

            // Rahmensetzung, damit Ball nicht aus Spielfeld fliegt
            if (player.getX() > Model.WIDTH && player.getSpeedX() > 0 || player.getX() < 0 && player.getSpeedX() < 0) {
                player.setSpeedX(-1 * player.getSpeedX());
            }

            if (player.getY() > Model.HEIGHT && player.getSpeedY() > 0 || player.getY() < 0 && player.getSpeedY() < 0) {
                player.setSpeedY(-1 * player.getSpeedY());
                spielende = true;

                System.out.println(ANSI_RED + "DU HAST LEIDER VERLOREN!");
            }
        }

        // Seitenkorrektur Paddle1
        int dx = Math.abs(player.getX() - paddle.getX());
        int dy = Math.abs(player.getY() - paddle.getY());
        int h = player.getH() / 2 + paddle.getH() / 2;
        int w;

        if (player.getX() < paddle.getX()) {
            w = player.getW();
        } else {
            w = paddle.getW() + player.getW();
        }

        if (dx <= w && dy <= h) {
            player.setSpeedY(-1 * player.getSpeedY());
        }


        // Korrektur für Paddle 2
        int ax = Math.abs(player.getX() - paddle2.getX());
        int ay = Math.abs(player.getY() - paddle2.getY());
        int b = player.getH() / 2 + paddle2.getH() / 2;


        if (player.getX() < paddle2.getX()) {
            w = player.getW();
        } else {
            w = paddle2.getW() + player.getW();
        }

        if (ax <= w && ay <= b) {
            player.setSpeedY(-1 * player.getSpeedY());


        }

    }
}