package model;

public class Paddle {

    private int x;
    private int y;
    private int h;
    private int w;
    private float speedX;


    public Paddle(int x, int y, int h, int w, float speedX) {
        this.x = x;
        this.y = y;
        this.h = h;
        this.w = w;
        this.speedX = speedX;
    }


    public void move(int dx, int dy) {
        this.x += dx;
        this.y += dy;

    }

    public void move2(int ax, int ay) {
        this.x += ax;
        this.y += ay;

    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }


    public float getSpeedX() {
        return speedX;
    }

    public void setSpeedX(float speedX) {
        this.speedX = speedX;
    }


    public void update(long elapsedTime) {
        this.x = Math.round(this.x + elapsedTime * this.speedX);
    }


}
